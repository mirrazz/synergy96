//!wrt $BSPEC:{"frn":"StudentVue","dsc":"A fully-fledged StudentVue client for Windows96"}

const {Theme}=w96.ui
const endpoint="https://synergy.awesomecrater.repl.co"

class StudentVue extends WApplication {
    constructor() { super() }
    async main(argv) {
        var mainwnd = this.createWindow({
            title: "StudentVue",
            body: `<div class="svue-body" style="width:100%;height:100%;flex-direction:column;display:flex;font-family:Arial,Helvetica,sans;">
                <div class="svue-titlebar" style="width:100%;background:purple;text-align:right;color:white;font-size:14px;">
                <p><span class="svue-win-min">_</span> <span class="svue-win-max">[ ]</span> <span class="svue-win-close">X</span></p>
                </div>
            </div>`,
            taskbar:true
        },true);
        var dom=mainwnd.wndObject;
        dom.querySelector(".svue-win-close").onclick=function() {
            mainwnd.close()
        }
        dom.querySelector(".svue-win-max").onclick=function() {
            mainwnd.toggleMaximize()
        }
        dom.querySelector(".svue-win-min").onclick=function() {
            mainwnd.toggleMinimize()
        }
        dom.querySelector(".titlebar").parentNode.removeChild(
            dom.querySelector('.titlebar')
        );
        dom.querySelector(".window-html-content").classList.remove('nodrag')
        mainwnd.show()
    }
}

return await WApplication.execAsync(new StudentVue(),this.boxedEnv.args,this)
